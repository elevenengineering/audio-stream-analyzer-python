# Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
# Copyright © 2017 Eleven Engineering Inc. Sam Cristall<cristall@eleveneng.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
import os, signal, sys, time, threading, numpy, traceback, imp
import sounddevice as sd
sys.path.append(os.path.join(os.path.dirname(__file__)))
import common.Algorithms as algo
from common.SampleSource import GenerateDiscreteSineFunction

kFramesPerBuffer = 512
kMaxTimeInSeconds = 10

class StreamDefinition:
  def __init__(self, def_string):
    args = def_string.split(';')
    if len(args) >= 2:
      self.device = int(args[0])
      self.channel = int(args[1])
    if len(args) == 3:
      self.freq = int(args[2])
    self.stream_string = def_string


class Output:
  def __init__(self, stream_def: StreamDefinition, sample_rate: float):
    def callback(
      data: numpy.ndarray,
      frames: int,
      time,
      status: sd.CallbackFlags
    ) -> None:
      nonlocal self
      data[:,self.channel] = self.out_arr[self.index:self.index+kFramesPerBuffer]
      self.index += frames

    self.device = stream_def.device
    self.channel = stream_def.channel
    self.stream_string = stream_def.stream_string
    freq = stream_def.freq

    self.correlations = {}
    self.index = 0
    out_gen = GenerateDiscreteSineFunction(freq, sample_rate)
    self.out_arr = numpy.fromfunction(out_gen, (int(sample_rate * kMaxTimeInSeconds),))

    self.callback = callback
    self.in_gen = GenerateDiscreteSineFunction(freq, sample_rate)
    in_arr = numpy.fromfunction(self.in_gen, (kFramesPerBuffer,))
    self.dft = algo.audio_dft(in_arr)
    algo.normalize_reduced_freq_signal_by_energy(self.dft)

  def register_input(self, stream_string):
    self.correlations[stream_string] = 0

  def start(self):
    self.stream.start()

  def abort(self):
    self.stream.abort()

class Input:
  def __init__(self, outputs, stream_def: StreamDefinition):
    self.device = stream_def.device
    self.channel = stream_def.channel
    self.stream_string = stream_def.stream_string
    self.data = numpy.empty((kFramesPerBuffer,), dtype = numpy.float32)
    self.outputs = outputs
    for output in outputs:
      output.register_input(self.stream_string)

    def callback(
      data: numpy.ndarray,
      frames: int,
      time,
      status: sd.CallbackFlags
    ) -> None:
      nonlocal self

      self.data[:] = data[:,self.channel]
      dft_arr = algo.audio_dft(self.data)
      algo.normalize_reduced_freq_signal_by_energy(dft_arr)
      for output in self.outputs:
        output.correlations[self.stream_string] = algo.get_cross_correlate_reduced_freq_energy_overlap(
                                            dft_arr, output.dft)

    self.callback = callback

  def start(self):
    self.stream.start()

  def abort(self):
    self.stream.abort()

class Stream:
  def __init__(self, device, sample_rate):
    self.sample_rate = sample_rate
    self.device = device
    self.out_channels = []
    self.out_max_channel = 0
    self.in_channels = []
    self.in_max_channel = 0

  def AddOutChannel(self, channel):
    self.out_channels.append(channel)
    if self.out_max_channel < channel.channel:
      self.out_max_channel = channel.channel

  def AddInChannel(self, channel):
    self.in_channels.append(channel)
    if self.in_max_channel < channel.channel:
      self.in_max_channel = channel.channel

  def CreateOutStream(self):
    def out_callback(
      data: numpy.ndarray,
      frames: int,
      time,
      status: sd.CallbackFlags
    ) -> None:
      nonlocal self
      for output in self.out_channels:
        output.callback(data, frames, time, status)

    print(self.out_max_channel)
    return sd.OutputStream(
            self.sample_rate,
            kFramesPerBuffer,
            self.device,
            self.out_max_channel+1,
            dtype = numpy.float32,
            callback = out_callback)

  def CreateInStream(self):
    def in_callback(
      data: numpy.ndarray,
      frames: int,
      time,
      status: sd.CallbackFlags
    ) -> None:
      nonlocal self
      for input in self.in_channels:
        input.callback(data, frames, time, status)

    return sd.InputStream(
        self.sample_rate,
        kFramesPerBuffer,
        self.device,
        self.in_max_channel+1,
        dtype = numpy.float32,
        callback = in_callback)



class DeviceManager:
  def __init__(self, outputs, inputs, sample_rate):
    stream_devices = []
    streams = []
    for stream in outputs + inputs:
      if not stream.device in stream_devices:
        stream_devices.append(stream.device)
        streams.append(Stream(stream.device, sample_rate))

    for output in outputs:
      for stream in streams:
        if output.device == stream.device:
          stream.AddOutChannel(output);

    for input in inputs:
      for stream in streams:
        if input.device == stream.device:
          stream.AddInChannel(input)
   
    self.out_streams = []
    for stream in streams:
      if len(stream.out_channels) > 0:
        self.out_streams.append(stream.CreateOutStream())

    self.in_streams = []
    for stream in streams:
      if len(stream.in_channels) > 0:
        self.in_streams.append(stream.CreateInStream())

  def start(self):
    for stream in self.out_streams:
      stream.start()
    for stream in self.in_streams:
      stream.start()

  def abort(self):
    for stream in self.in_streams:
      try:
        stream.abort()
      except:
        pass
    for stream in self.out_streams:
      try:
        stream.abort()
      except:
        pass


def RunAnalyzer(out_stream_defs, in_stream_defs, sample_rate: float, stabilization_time: float):
    result = -1
  #try:
    outputs = []
    inputs = []

    for stream_def in out_stream_defs:
      outputs.append(Output(stream_def, sample_rate))

    for stream_def in in_stream_defs:
      inputs.append(Input(outputs, stream_def))

    dev_mngr = DeviceManager(outputs, inputs, sample_rate)
    dev_mngr.start()

    time.sleep(stabilization_time)

    result = outputs

  #except:
  #  pass
  #finally:
    dev_mngr.abort()
    return result

def GetDefaultSampleRate(device_num):
  return sd.query_devices()[device_num]['default_samplerate']

def IsDevicePresent(device_name):
  dev_list = sd.query_devices()
  for dev in dev_list:
    if device_name in dev['name']:
      return True
  return False

def RestartSounddevice():
  sd._terminate()
  sd._initialize()

def ExpectedStreamHasTone(outputs, success_correlation, expected):
  for output in outputs:
    print('{"%s;%s" : %s}' % (output.device, output.channel, output.correlations))
    if all(corr > success_correlation for _,corr in output.correlations.items()):
      return expected == output.stream_string
  return False


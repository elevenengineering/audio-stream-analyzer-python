# Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import math
from functools import reduce
import numpy as np


def audio_dft(arr: np.array) -> np.array:
  return np.fft.rfft(arr, norm = 'ortho')


def energy_atom_func(v) -> float:
  return float(np.absolute(v*v))


# Computes the energy of a signal.
def get_signal_energy(arr: np.array) -> float:
  return reduce(lambda v0, v1: v0 + energy_atom_func(v1), np.nditer(arr), 0.0)


# Computes the energy of a reduced frequency-domain signal from a fft.rfft.
def get_reduced_freq_signal_energy(arr: np.array) -> float:
  dc_energy = energy_atom_func(arr[0])
  final_ac_energy = energy_atom_func(arr[-1])
  ac_energy = 2.0 * get_signal_energy(arr[1:-1])
  return dc_energy + ac_energy + final_ac_energy


def generic_normalize_signal_by_energy(arr: np.array, energy_func) -> None:
  norm_factor = math.sqrt(energy_func(arr))
  if norm_factor == 0.0:
    return
  np.divide(arr, norm_factor, arr)


# Normalizes a time-domain or FULL frequency-domain signal so that its total
# energy is 1.
def normalize_signal_by_energy(arr: np.array) -> None:
  return generic_normalize_signal_by_energy(arr, get_signal_energy)


# Normalizes a reduced frequency-domain signal so that its total energy is 1.
def normalize_reduced_freq_signal_by_energy(arr: np.array) -> None:
  return generic_normalize_signal_by_energy(arr, get_reduced_freq_signal_energy)


def cross_correlation_atom_func(v0, v1):
  return v0*np.conj(v1)


# Computes the spectral energy overlap of two reduced frequency-domain signals
# using their cross-correlation. This can be at most the square of the product
# of the signals' energy (if it is an autocorrelation), and no less than 0 if
# there is absolutely no overlap between signals.
# Both signals must have the same length.
def get_cross_correlate_reduced_freq_energy_overlap(
  arr0: np.array,
  arr1: np.array
) -> float:
  abs_cc = lambda v0, v1: np.absolute(cross_correlation_atom_func(v0, v1))
  sum_f = lambda accum, tup: accum + abs_cc(tup[0], tup[1])
  dc_overlap = abs_cc(arr0[0], arr1[0])
  final_ac_overlap = abs_cc(arr0[-1], arr1[-1])
  ac_overlap = 2.0 * reduce(sum_f, zip(arr0[1:-1], arr1[1:-1]), 0.0)
  return energy_atom_func(dc_overlap + final_ac_overlap + ac_overlap)

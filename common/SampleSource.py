# Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import numpy, math
from typing import Callable


# Generates a discrete sequence of floating-point sinusoidal values
# corresponding to the provided frequency and sample rate.
# Subject to the Nyquist sampling theorem, and will generated an aliased
# sequence if the frequency is greater than 2*sample_rate.
def GenerateDiscreteSineFunction(
  frequency: float,
  sample_rate: float,
  scalar: float = 1.0,
  initial_angle: float = 0.0
) -> Callable:
  return lambda n: scalar * numpy.sin(2*math.pi*n*frequency/sample_rate + initial_angle)
